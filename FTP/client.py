import socket			 
import sys
import threading
from os import _exit

CHUNK_SIZE = 8 * 1024

s = socket.socket()

spIndex = sys.argv.index('-sp')
sipIndex = sys.argv.index('-sip')
port = int(sys.argv[spIndex+1])
ip = sys.argv[sipIndex+1]

s.connect((ip, port))
print(s.recv(1024).decode())

def clientLoop() :
    while True :
        fileName = input()
        s.send(fileName.encode())
        
        if fileName == "__exit__" :
            _exit(True)
            break

        if fileName.split()[0] == 'List' :
            print(s.recv(1024).decode())
            continue

        if fileName.split()[0] == 'Delete' :
            data = s.recv(1024)
            if data == b'__NotFound__' :
                print("File Not Found!")
            else :
                print("Done!")
            continue

        data = s.recv(CHUNK_SIZE)
        if data == b'__NotFound__' :
            print("File Not Found!")
            continue

        with open("clientMemmory/"+fileName, "wb") as f :
            while data :
                f.write(data)
                data = s.recv(CHUNK_SIZE)
                if data == b'__EOF__' :
                    break
        print("Done!")

threading.Thread(target=clientLoop).start()