import socket			 
import sys
import threading
from os import _exit
from time import sleep
from os.path import exists
from os import listdir
from os import system

CHUNK_SIZE = 8 * 1024

s = socket.socket()		 

spIndex = sys.argv.index('-sp')
port = int(sys.argv[spIndex+1])
ipAdr = '192.168.43.237'

s.bind((ipAdr, port))
print ("socket server binded to %s:%d" %(ipAdr, port))

s.listen(5)	 
print ("server is listening")			


clients = {}
def newClientLoop() :
    while True :
        newClient, newAddr = s.accept()
        print ('Got connection from', newAddr)
        massege = 'Thank you for connecting. address = '+str(newAddr)
        newClient.send(massege.encode())
        clients[newAddr] = newClient
        threading.Thread(target=responseLoop, args=(newAddr,)).start()

def responseLoop(address) :
    while True :
        fileName = clients[address].recv(1024).decode()
        
        if not fileName or fileName == '__exit__' :
            fileName = 'One lost connection!'
            break

        if fileName.split()[0] == 'List' :
            print(str(listdir("serverMemmory/")))
            clients[address].send(str(listdir("serverMemmory/")).encode())
            continue
        
        if fileName.split()[0] == 'Delete' :
            fileName = fileName[7:]
            if not exists("serverMemmory/"+fileName) :
                clients[address].send(b'__NotFound__')
                print("File Not Found!")
            else :
                system("rm ./serverMemmory/"+fileName)
                clients[address].send(b'__Done__')
                print(fileName+" Deleted!")
            continue
        
        if not exists("serverMemmory/"+fileName) :
            clients[address].send(b'__NotFound__')
            print("File Not Found!")
            continue
    
        with open("serverMemmory/"+fileName, "rb") as f :
            data = f.read(CHUNK_SIZE)
            while data :
                clients[address].send(data)
                data = f.read(CHUNK_SIZE)
        sleep(0.001)
        clients[address].send(b'__EOF__')

    del clients[address]

def inputLoop() :
    while True:
        command = input()
        if command == '__exit__' :
            _exit(True)
        elif command == 'list' :
            for adr, client in clients.items() :
                print(adr, ':', client)
            print('number of clients:', len(clients))

threading.Thread(target=newClientLoop).start()
threading.Thread(target=inputLoop).start()