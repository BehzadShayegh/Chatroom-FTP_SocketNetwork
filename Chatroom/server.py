import socket			 
import sys
import threading
from os import _exit
from time import sleep

s = socket.socket()		 

spIndex = sys.argv.index('-sp')
port = int(sys.argv[spIndex+1])
ipAdr = '127.0.0.1'

s.bind((ipAdr, port))
print ("socket server binded to %s:%d" %(ipAdr, port))

s.listen(5)	 
print ("server is listening")		


clients = {}
def newClientLoop() :
    while True :
        newClient, newAddr = s.accept()
        print ('Got connection from', newAddr)
        massege = 'Thank you for connecting. address = '+str(newAddr)
        newClient.send(massege.encode())
        clients[newAddr] = newClient
        threading.Thread(target=massegesLoop, args=(newAddr,)).start()

def massegesLoop(address) :
    alive = True
    while alive :
        clientMassege = clients[address].recv(1024).decode()
        if not clientMassege or clientMassege == '__exit__' :
            clientMassege = 'Left the group!'
            alive = False
        massege = '<From '+str(address)+'> '+clientMassege
        print(massege)
        for other in clients :
            if other == address : continue
            clients[other].send(massege.encode())
    del clients[address]

def inputLoop() :
    while True:
        command = input()
        if command == '__exit__' :
            _exit(True)
        elif command == 'list' :
            for adr, client in clients.items() :
                print(adr, ':', client)
            print('number of clients:', len(clients))

threading.Thread(target=newClientLoop).start()
threading.Thread(target=inputLoop).start()
