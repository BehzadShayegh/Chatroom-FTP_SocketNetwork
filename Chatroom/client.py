import socket			 
import sys
import threading
from os import _exit

s = socket.socket()		 

spIndex = sys.argv.index('-sp')
sipIndex = sys.argv.index('-sip')
port = int(sys.argv[spIndex+1])
ip = sys.argv[sipIndex+1]

s.connect((ip, port))
print(s.recv(1024).decode())

def recvLoop() :
    while True :
        serverMassege = s.recv(1024).decode()
        print(serverMassege)

def sendLoop() :
    while True :
        clientMassege = input()
        s.send(clientMassege.encode())
        if clientMassege == "__exit__" :
            _exit(True)

threading.Thread(target=recvLoop).start()
threading.Thread(target=sendLoop).start()